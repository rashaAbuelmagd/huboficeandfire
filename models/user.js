'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  
  User.associate = function (models) {
    User.hasMany(models.Post, {
      foreignKey: 'UserId',
      as: 'posts'
    });
  };
  return User;
};