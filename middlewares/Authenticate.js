'use strict'

const jwt = require('jsonwebtoken');


exports.Authenticate = function (req, res, next) {

  const token = req.headers.authorization

  if (!token) {
    return res.status(400).send({
      "success": false,
      error: {
        message: "Token not found in request headers",
      }
    })
  }

  try {

    const user = jwt.verify(token, 'AnIsR&CJ9bX8ys0hlA')

    res.locals.user = user;

    next();

  } catch (error) {
    return res.status(401).send({
      "success": false,
      "error": {
        message: "invalid api token",
        data: error
      }
    })
  }
}