const models = require('../models/')
const Sequelize = require('sequelize')

exports.getByUserId = async (userId) => {

    const posts = await models.Post.findAll({
        where: { UserId: userId },
        attributes: ['body'],
        order: [['createdAt', 'DESC']]
    });

    return posts

}

exports.getRecent = async () => {

    const posts = await models.Post.findAll({
        include: [{
            model: models.User,
            attributes: ['username'],
        }],
        attributes: ['body', 'UserId'],
        order: [['createdAt', 'DESC']]
    });

    return posts;

}

exports.getByHashtag = async (hashtag) => {

    const posts = await models.Post.findAll({
        where: {
            [Sequelize.Op.or]: [{
                "hashtags": {
                    [Sequelize.Op.like]: hashtag + '%'
                }
            }, {
                "hashtags": {
                    [Sequelize.Op.like]: '%' + hashtag + '%'
                }
            }]
        }
    })

    return posts;
}

exports.store = async (postData) => {

    return await models.Post.create(postData)

}

exports.getByUsername = async (username, userId) => {

    const posts = await models.Post.findAll({
        include: [{
            model: models.User,
            attributes: ['username'],
            where: { username: username }
        }],
        attributes: ['body', 'UserId'],
        where: { UserId: userId },
        order: [['createdAt', 'DESC']]
    });

    return posts
}