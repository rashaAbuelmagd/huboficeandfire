const express = require('express');
const router = express.Router();

const PostController = require('../controllers/postController');
const middleware = require('../middlewares/Authenticate');
const postValidator = require('../validations/post');

router.post('/', middleware.Authenticate, postValidator.validate('create'), PostController.create);
router.get('/hashtag/:hashtag', middleware.Authenticate, PostController.fillterByHashtag);
router.get('/userCreated', middleware.Authenticate, PostController.getByCurrentUser);
router.get('/recent', middleware.Authenticate, PostController.getMostRecent);
router.get('/username/:username', middleware.Authenticate, PostController.getByUsername);

module.exports = router;
