const nodeMailer = require('nodemailer');

let transporter = nodeMailer.createTransport({
    from: '"Hub Of Ice And Fire" <' + process.env['MAIL_FROM'] + '>',
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: process.env['MAIL_FROM'],
        pass: process.env['MAIL_FROM_PWD']
    }
});

exports.sendNotification = (userMail, userId, post) => {

    let mailOptions = {
        to: userMail,
        subject: 'Hub Of Ice And Fire',
        html: '<b>' + userId + ' Mentioned You in post ' + post + '</b>'
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            res.send('There was an error sending the email');
            return;
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.send('Email Sent');
    });
    
};