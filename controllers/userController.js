const bcrypt = require("bcrypt")
const jwt = require('jsonwebtoken')
const { validationResult } = require('express-validator')
const UserService = require('../services/userService')

/**
   * @api {post} /users Register New User
   * @apiGroup User
   * @apiVersion 1.0.0
   * @apiParam {String} body Post Body.
   * @apiParamExample {json} Request-Example:
   *     {
   *        "firstname":"Test",
   *        "lastname":"Test1",
   *        "username":"testTest",
   *        "email":"test@test.com",
   *        "password":"test123"
   *      }
   * @apiHeader {String} Content-Type application/json.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Content-Type":"application/json",
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   *   {
   *    "message": "account Created Successfully",
   *    "data": {
   *        "id": 4,
   *        "firstname": "Test",
   *        "lastname": "Test1",
   *        "username": "testTest",
   *        "email": "test@test.com",
   *        "password": "$2b$08$2e0y2NkcBzCAGiOyAxYwDuLPRP/OV4VBTXe3Ny5keQ1EUDxD26JkS",
   *        "updatedAt": "2019-08-22T15:53:13.785Z",
   *        "createdAt": "2019-08-22T15:53:13.785Z"
   *    }
   *   }
   */
exports.create = async (req, res) => {

    const userData = req.body;

    userData.password = await bcrypt.hash(userData.password, bcrypt.genSaltSync(8));

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    try {
        const user = await UserService.store(userData)

        res.status(200).json({
            message: 'Account Created Successfully',
            data: user
        });

    } catch (error) {
        res.status(500).json({
            message: 'Error in saving data',
            data: error
        });
    }

};

/**
   * @api {post} /users/login User Login
   * @apiGroup User
   * @apiVersion 1.0.0
   * @apiParam {String} email
   * @apiParam {String} password
   * @apiParamExample {json} Request-Example:
   *     {
   *        "email": "test@test.com"
   *        "password": "test123"
   *     }
   *
   * @apiHeader {String} Content-Type application/json.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Content-Type":"application/json",
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   *  {
   *  "message": "Logged In Successfully",
   *  "data": {
   *     "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsInVzZXJuYW1lIjoibmFzaHdhTW9oYW1lZCIsImlhdCI6MTU2NjQ4OTMwMH0.YtV_NU5Mq1yGyqHNvTky8KE0tYKSZ82dykdS1D_zj3s",
   *     "user": {
   *          "id": 4,
   *         "firstname": "Test",
   *          "lastname": "Test1",
   *          "username": "testTest",
   *          "email": "test@test.com",
   *          "password": "$2b$08$2e0y2NkcBzCAGiOyAxYwDuLPRP/OV4VBTXe3Ny5keQ1EUDxD26JkS",
   *          "createdAt": "2019-08-22T15:53:13.785Z",
   *          "updatedAt": "2019-08-22T15:53:13.785Z"
   *      }
   *  }
   * }
   * 
   */
exports.login = async (req, res) => {

    const loginData = req.body

    const user = await UserService.getByEmail(loginData.email)

    if (!user) {
        return res.status(404).json({
            message: 'Email not found',
            data: []
        });
    }

    const validPassword = await bcrypt.compare(loginData.password, user.password);

    if (!validPassword) {
        return res.status(400).json({
            message: 'Wrong password',
            data: []
        });
    }
    const token = jwt.sign({ userId: user.id, username: user.username }, process.env['APP_KEY']);

    res.status(200).json({
        message: 'Logged In Successfully',
        data: { "accessToken": token, "user": user }
    });

};

/**
   * @api {get} /users/:username Get User Profile
   * @apiGroup User
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "User Profile getted successfully",
   *  "data": {
   *     "firstname": "Test",
   *     "lastname": "Test1",
   *     "username": "testTest",
   *     "email": "test@test.com"
   *    }
   * }
   * @apiError UserNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "User Not Found",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.get = async (req, res) => {

    const username = req.params.username

    const user = await UserService.getByUserName(username)

    if (!user) {
        return res.status(404).json({
            message: 'User Not Found',
            data: []
        });
    }

    res.status(200).json({
        message: 'User Profile getted successfully',
        data: user
    });
}