const { validationResult } = require('express-validator')

const mailerHelper = require('../helpers/mailer')
const UserService = require('../services/userService')
const PostService = require('../services/postService')

/**
   * @api {post} /posts Create new post
   * @apiGroup Post
   * @apiVersion 1.0.0
   * @apiParam {String} body Post Body.
   * @apiParamExample {json} Request-Example:
   *     {
   *        "body": "Test Test test #test #testHashtag"
   *     }
   *
   * @apiHeader {String} Content-Type application/json.
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Content-Type":"application/json",
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Your Post Created Successfully",
   *  "data": {
   *     "id": 13,
   *     "body": "Test Test test #test #testHashtag",
   *     "hashtags": "#test",
   *     "UserId": 3,
   *     "updatedAt": "2019-08-22T09:59:08.843Z",
   *     "createdAt": "2019-08-22T09:59:08.843Z"
   *   }
   * }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   * 
   */
exports.create = async (req, res) => {

    const user = res.locals.user;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    let postData = req.body;
    let hashtags = postData.body.match(/#(.*?)[\s]+/gi);
    let mentions = postData.body.match(/@(.*?)[\s]+/gi);

    if (hashtags)
        hashtags = hashtags.toString();

    if (mentions) {
        sendMail(mentions, user.username, postData.body);
    }

    postData.hashtags = hashtags;

    postData.UserId = user.userId;

    try {
        const post = await PostService.store(postData)

        res.status(200).json({
            message: 'Your Post Created Successfully',
            data: post
        });

    } catch (error) {
        res.status(500).json({
            message: err.message,
            data: []
        });
    }

};

/**
   * @api {get} /posts/hashtag/:hashtag Fillter Posts By Hashtag
   * @apiGroup Post
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Post matches hashtag",
   *  "data": [
   *    {
   *       "id": 1,
   *       "body": "rsdasoldjsudicnjdshf ddnuiasdnioasndias #rasdha #trtrt dubiasjkdniUANSUOAJdkl #sids @rasha",
   *       "hashtags": "#rasdha,#trtrt,#sids",
   *       "UserId": 123,
   *       "createdAt": "2019-08-19T07:06:36.982Z",
   *       "updatedAt": "2019-08-19T07:06:36.982Z"
   *     }
   *  ]
   * }
   * @apiError PostsNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "There are no posts matched your search",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.fillterByHashtag = async (req, res) => {
    const posts = await PostService.getByHashtag(req.params.hashtag)

    if (!posts) {
        return res.status(404).json({
            message: 'There are no posts matched your search',
            data: []
        });
    }

    res.status(200).json({
        message: 'Post matches hashtag',
        data: posts
    });
};

/**
   * @api {get} /posts/recent/ View Most Recent Posts
   * @apiGroup Post
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Most Recent Posts",
   *  "data": [
   *    {
   *       "body": "ccsdcsd",
   *       "UserId": 123,
   *       "User": test
   *    },
   *   {
   *      "body": "rsdasoldjsudicnjdshf ddnuiasdnioasndias #rasdha #trtrt dubiasjkdniUANSUOAJdkl #sids @rasha",
   *      "UserId": 123,
   *      "User": test
   *   }
   *  ]
   * }
   * @apiError PostsNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "There are no posts matched your search",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.getMostRecent = async (req, res) => {

    const posts = await PostService.getRecent();

    if (!posts) {
        return res.status(404).json({
            message: 'There are no posts matched your search',
            data: []
        });
    }

    res.status(200).json({
        message: 'Most Recent Posts',
        data: posts
    });

};

/**
   * @api {get} /posts/userCreated View posts he/she created.
   * @apiGroup Post
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Posts User Created",
   *  "data": [
   *    {
   *       "body": "rsdasoldjsudicnjdshf ddnuiasdnioasndias #rasdha #trtrt dubiasjkdniUANSUOAJdkl #sids @rasha",
   *    },
   *    {
   *       "body": "Test Test test #test",
   *    }
   *  ]
   * }
   * @apiError PostsNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "There are no posts matched your search",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.getByCurrentUser = async (req, res) => {

    const user = res.locals.user;

    const posts = await PostService.getByUserId(user.userId)

    if (!posts) {
        return res.status(404).json({
            message: 'There are no posts matched your search',
            data: []
        });
    }

    res.status(200).json({
        message: 'Posts User Created',
        data: posts
    });
};

/**
   * @api {get} /posts/username/:username View posts created by any other user. 
   * @apiGroup Post
   * @apiVersion 1.0.0
   * @apiHeader {String} Authorization unique access-key.
   * @apiHeaderExample {json} Header-Example:
   *     {
   *       "Authorization":"kkNRuRurO0lX7N24"
   *     }
   * @apiSuccessExample {json} Success-Response:
   *   HTTP/1.1 200 OK
   * {
   *  "message": "Posts User Created",
   *  "data": [
   *    {
   *       "body": "rsdasoldjsudicnjdshf ddnuiasdnioasndias #rasdha #trtrt dubiasjkdniUANSUOAJdkl #sids @rasha",
   *       "UserId": 123,
   *       "User": {
   *                  "username": "rashaAbuelmagd"
   *               }
   *     }
   *  ]
   * }
   * @apiError PostsNotFound
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "message": "There are no posts matched your search",
   *       "data":[]
   *     }
   * @apiError AuthError
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 400 Bad Request
   *     "success": false,
   *     "error":{
   *       "message": "Token not found in request headers",
   *     }
   */
exports.getByUsername = async (req, res) => {

    const username = req.params.username;
    const user = res.locals.user;

    const posts = await PostService.getByUsername(username, user.userId)

    if (!posts) {
        return res.status(404).json({
            message: 'There are no posts matched your search',
            data: []
        });
    }

    res.status(200).json({
        message: 'Posts User Created',
        data: posts
    });
};

/**
 * Get mail for username then send mail 
 * @param {*} usernames 
 * @param {*} userId 
 * @param {*} postBody 
 */
const sendMail = async (usernames, userId, postBody) => {

    usernames = usernames.map(username => username.slice(1,-1));

    const users = await UserService.getAllUserNames(usernames)

    if (!users)
        return;

    users.forEach(user => {
        mailerHelper.sendNotification(user.email, userId, postBody)
    });

}
