
module.exports = {
  apps: [{
    name: 'huboficeandfire',
    script: 'bin/www',
    instances: 1,
    autorestart: true,
    max_memory_restart: '1G'
  }]
}