const { check } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                check('body', 'Please provide the post body with max length 140 character').not().isEmpty().isLength({ max: 140 })
            ]
        }
    }
}